# -*- coding: utf-8 -*-
'''
Surprisingly there are only three numbers that can be written as the sum of fourth powers of their digits:

1634 = 14 + 64 + 34 + 44
8208 = 84 + 24 + 04 + 84
9474 = 94 + 44 + 74 + 44
As 1 = 14 is not a sum it is not included.

The sum of these numbers is 1634 + 8208 + 9474 = 19316.

Find the sum of all the numbers that can be written as the sum of fifth powers of their digits.
'''

if __name__ == '__main__':
    dfpSum = 0
    power  = 5
    SUP    = (power + 1) * 9 ** power
    for x in xrange(2, SUP):
        tempSum = 0
        for y in str(x):
            tempSum += int(y) ** power
        if tempSum == x:
            dfpSum += x

    print dfpSum
