# -*- coding: utf-8 -*-

def fibonacci_iterative(first, second):
    first, second = second, first + second
    return (first, second)

def fibonacci_sup(intNum):
    first, second = 1, 1
    fibList = list((first, second))
    while second <= intNum:
        first, second = fibonacci_iterative(first, second)
        fibList.append(second)
    else:
        fibList.pop()
    return fibList

if __name__ == '__main__':
    print fibonacci_sup(100)
