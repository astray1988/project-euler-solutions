# -*- coding: utf-8 -*-
'''
The series, 11 + 22 + 33 + ... + 1010 = 10405071317.

Find the last ten digits of the series, 11 + 22 + 33 + ... + 10001000.
'''

if __name__ == '__main__':
    tenSum = 0
    for x in xrange(1, 1001):
        tenSum += int(str(x ** x)[-10:])
    print str(tenSum)[-10:]
