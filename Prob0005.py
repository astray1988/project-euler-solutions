# -*- coding: utf-8 -*-
'''
2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.

What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
'''

from Primes import get_lcm

if __name__ == '__main__':
    print get_lcm(*range(1, 21))
