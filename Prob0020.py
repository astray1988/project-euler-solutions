# -*- coding: utf-8 -*-
'''
n! means n × (n − 1) × ... × 3 × 2 × 1

For example, 10! = 10 × 9 × ... × 3 × 2 × 1 = 3628800,
and the sum of the digits in the number 10! is 3 + 6 + 2 + 8 + 8 + 0 + 0 = 27.

Find the sum of the digits in the number 100!
'''

from Factorial import factorial as f

if __name__ == '__main__':
    digSum = 0
    for x in str(f(100)):
        digSum += int(x)
    print digSum
