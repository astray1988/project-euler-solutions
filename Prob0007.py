# -*- coding: utf-8 -*-
'''
By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.

What is the 10 001st prime number?
'''

from Primes import prime_test as pt

if __name__ == '__main__':
    count = 0
    working = 1
    while count < 10001:
        working += 1
        if pt(working):
            count += 1
    print working
