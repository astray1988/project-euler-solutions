# -*- coding: utf-8 -*-
'''
The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.

Find the sum of all the primes below two million.
'''

from Primes import prime_test as pt
def main():
    working = 2
    SUP = 2 * 10 ** 6
    primeSum = 0
    while working < SUP:
        if pt(working):
            primeSum += working
        working += 1
    print primeSum

if __name__ == '__main__':
    main()
