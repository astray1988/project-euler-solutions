# -*- coding: utf-8 -*-

def prime_test(num):
    if num == 1:
        return False
    for i in range(2, 1 + int(num ** 0.5)):
        if num % i == 0:
            return False
    return True

def get_prime_factors(num):
    if num == 1:
        return None
    working = 2
    primeFactorsDict = dict()
    while working ** 2 <= num:
        count = 0
        while not num % working:
            count += 1
            num //= working
        else:
            if count:
                primeFactorsDict[working] = count
            working += 1
    else:
        if num > 1:
            primeFactorsDict[num] = 1
        return primeFactorsDict

def num_of_factors(num):
    if num == 1:
        return 1
    working = 2
    nof = 1
    while working ** 2 <= num:
        current = 0
        while not num % working:
            current += 1
            num //= working
        nof *= (current + 1)
        working += 1
    if num > 1:
        nof *= 2
    return nof

def get_proper_factors_div(intNum):
    if intNum == 1:
        return None
    root     = int(intNum ** 0.5) + 1
    storeSet = set()
    storeSet.add(1)
    for i in xrange(2, root):
        if not intNum % i:
            storeSet.add(i)
            storeSet.add(intNum / i)
    return sorted(storeSet)

def get_proper_factors(intNum):
    if intNum == 1:
        return None
    storeList = list()
    handle_prime_factors(get_prime_factors(intNum), 1, storeList)
    return sorted(storeList[:-1])

def handle_prime_factors(workingDict, res, storeList):
    if not len(workingDict):
        storeList.append(res)
        return storeList
    currentKey = workingDict.keys()[0]
    for i in range(workingDict[currentKey] + 1):
        temp = currentKey ** i
        handle_prime_factors(dict([(key, workingDict[key])for key in workingDict.keys() if not key == currentKey]), res * temp, storeList)

def get_lcm(*numList):
    storeDict = dict()
    for x in numList:
        workingDict = get_prime_factors(x)
        if not workingDict:
            pass
        else:
            for prime, power in workingDict.items():
                if storeDict.has_key(prime):
                    storeDict[prime] = max(storeDict[prime], workingDict[prime])
                else:
                    storeDict.update({prime : power})
    prod = 1
    for prime, power in storeDict.items():
        prod *= prime ** power
    return prod

def get_gcd(*numList):
    working = 1
    for x in numList:
        working *= x
    return working / get_lcm(*numList)

if __name__ == '__main__':
    testNum = 36
    SUP     = 28124
    for x in xrange(2, SUP):
        a = get_proper_factors_div(x)
        b = get_proper_factors(x)
        if a != b:
            print x, '\t', a, '\t', b
